package com.example.edwin.audioplayertutorial.Utility;

import android.view.View;

/**
 * Created by edwin on 19/09/17.
 */

public interface onItemClickListener {

    public void onClick(View view, int index);
}
