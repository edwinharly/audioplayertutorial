package com.example.edwin.audioplayertutorial.Module.Authentication;

import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.edwin.audioplayertutorial.R;

public class AuthActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);

        Fragment fragment = new LoginFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.fragment_container, fragment).commit();


    }
}
