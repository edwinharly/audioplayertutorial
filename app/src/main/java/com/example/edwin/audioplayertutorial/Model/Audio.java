package com.example.edwin.audioplayertutorial.Model;

import java.io.Serializable;

/**
 * Created by edwin on 17/09/17.
 */

// CLASS INI LAH BLUEPRINT NYA SUATU AUDIO (MUSIK), TAG, GENRE, MOOD, DLL MUNGKIN NANTI AKAN
// DITAMBAHKAN INSTANCE VARIABLE YG DIPERLUKAN OLEH SISTEM KE DALAM CLASS INI

public class Audio implements Serializable
{
    private String data;
    private String title;
    private String album;
    private String artist;

    public Audio(String data, String title, String album, String artist) {
        this.data = data;
        this.title = title;
        this.album = album;
        this.artist = artist;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }
}
