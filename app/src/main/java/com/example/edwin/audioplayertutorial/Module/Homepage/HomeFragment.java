package com.example.edwin.audioplayertutorial.Module.Homepage;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.edwin.audioplayertutorial.Module.PlaylistCardview.Adapter.RecyclerViewAdapter;
import com.example.edwin.audioplayertutorial.Module.PlaylistCardview.Model.Playlist;
import com.example.edwin.audioplayertutorial.R;

import java.util.ArrayList;

/**
 * Created by denny on 27/10/17.
 */

public class HomeFragment extends android.app.Fragment {
    RecyclerView mRecyclerView;
    RecyclerViewAdapter mAdapterRV;
    TextView titleRV;
    ArrayList<Playlist> playlists = new ArrayList<>();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        playlists.add(new Playlist("Rock & Roll", R.drawable.playlist_img,
                "Kompilasi lagu Rock&Roll terbaru untuk anda"));
        playlists.add(new Playlist("Rock & Roll", R.drawable.playlist2,
                "Kompilasi lagu Rock&Roll terbaru untuk anda"));
        playlists.add(new Playlist("Rock & Roll", R.drawable.playlist3,
                "Kompilasi lagu Rock&Roll terbaru untuk anda"));
        playlists.add(new Playlist("Rock & Roll", R.drawable.playlist4,
                "Kompilasi lagu Rock&Roll terbaru untuk anda"));
        playlists.add(new Playlist("Rock & Roll", R.drawable.playlist5,
                "Kompilasi lagu Rock&Roll terbaru untuk anda"));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.home_fragment, container, false);

        /**
         * TODO: tambahkan Playlist Recyclerview dengan method ini
         */
        setPlaylistCardviewTitle(view, R.id.title_recommended, "Made for you");
        createPlaylistCardview(view, R.id.recyclerview_recommended, playlists);

        return view;
    }

    /**
     * membuat sebuah recyclerview dari playlist
     * @param view
     * @param recyclerViewId
     * @param dataPlaylists
     */
    private void createPlaylistCardview(View view, int recyclerViewId, ArrayList<Playlist> dataPlaylists){
        mRecyclerView = view.findViewById(recyclerViewId);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL, false));
        mAdapterRV = new RecyclerViewAdapter(dataPlaylists,getActivity());
        mRecyclerView.setAdapter(mAdapterRV);
    }

    /**
     * membuat judul untuk RVnya
     * @param view
     * @param textViewId
     * @param text
     */
    private void setPlaylistCardviewTitle(View view, int textViewId, String text){
        titleRV = view.findViewById(textViewId);
        titleRV.setText(text);
    }
}
