package com.example.edwin.audioplayertutorial.Module.PlaylistCardview.Model;

/**
 * Created by denny on 31/10/17.
 */

public class Playlist {

    private String title;
    private int picture;
    private String explanation;

    public Playlist(String title, int picture, String explanation) {
        this.title = title;
        this.picture = picture;
        this.explanation = explanation;
    }

    public String getTitle() {
        return title;
    }

    public int getPicture() {
        return picture;
    }

    public String getExplanation() {
        return explanation;
    }
}
