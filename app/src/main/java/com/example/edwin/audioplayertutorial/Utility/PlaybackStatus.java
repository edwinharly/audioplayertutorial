package com.example.edwin.audioplayertutorial.Utility;

/**
 * Created by edwin on 19/09/17.
 */

public enum PlaybackStatus {
    PLAYING,
    PAUSED
}
