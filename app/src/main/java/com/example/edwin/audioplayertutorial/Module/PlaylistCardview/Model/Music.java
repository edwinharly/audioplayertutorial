package com.example.edwin.audioplayertutorial.Module.PlaylistCardview.Model;

import java.io.Serializable;

/**
 * Created by denny on 31/10/17.
 */

public class Music implements Serializable{

    private String title;
    private String explanation;
    private int photoResourceId;

    public Music(String title, String explanation, int photoResourceId) {
        this.title = title;
        this.explanation = explanation;
        this.photoResourceId = photoResourceId;
    }

    public String getTitle() {
        return title;
    }

    public String getExplanation() {
        return explanation;
    }

    public int getPhotoResourceId() {
        return photoResourceId;
    }
}
