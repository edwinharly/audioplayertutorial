package com.example.edwin.audioplayertutorial.Module.PlaylistCardview.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.edwin.audioplayertutorial.Module.PlaylistCardview.Model.Playlist;
import com.example.edwin.audioplayertutorial.R;

import java.util.ArrayList;

/**
 * Created by denny on 30/10/17.
 */

public class RecyclerViewAdapter extends RecyclerView.Adapter<CustomViewHolder> {

    private ArrayList<Playlist> playlists;
    private Context context;

    public RecyclerViewAdapter(ArrayList<Playlist> _playlists, Context _context) {
        this.playlists = _playlists;
        this.context = _context;
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //Inflate the layout, initialize the View Holder
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.playlist_cardview, parent, false);
        return new CustomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CustomViewHolder holder, int position) {

        holder.cardViewTitle.setText(playlists.get(position).getTitle());
        holder.cardViewImage.setImageResource(playlists.get(position).getPicture());
        holder.cardViewExplanation.setText(playlists.get(position).getExplanation());
    }

    @Override
    public int getItemCount() {
        return playlists.size();
    }
}

class CustomViewHolder extends RecyclerView.ViewHolder{

    ImageButton cardViewImage;
    TextView cardViewTitle;
    TextView cardViewExplanation;

    public CustomViewHolder(View itemView) {
        super(itemView);
        cardViewImage = itemView.findViewById(R.id.cardview_image);
        cardViewTitle = itemView.findViewById(R.id.cardview_title);
        cardViewExplanation = itemView.findViewById(R.id.cardview_explanation);
    }
}