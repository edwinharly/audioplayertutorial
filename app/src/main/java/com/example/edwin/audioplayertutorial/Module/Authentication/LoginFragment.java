package com.example.edwin.audioplayertutorial.Module.Authentication;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.edwin.audioplayertutorial.Module.Homepage.MainActivity;
import com.example.edwin.audioplayertutorial.R;
import com.example.edwin.audioplayertutorial.Utility.SessionManager;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

/**
 * Created by denny on 29/09/17.
 */

public class LoginFragment extends Fragment {

    CallbackManager callbackManager;
    AccessToken accessToken;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.login_form, container, false);
        final Button btnToRegister = view.findViewById(R.id.button_to_register);
        final Button btnToLogin = view.findViewById(R.id.button_to_login);

        // facebook login button
        final LoginButton loginButton = view.findViewById(R.id.login_button);
        loginButton.setReadPermissions("email");
        //  jika menggunakan fragment
        loginButton.setFragment(this);
        callbackManager = CallbackManager.Factory.create(); // to handle login responses


        final EditText txtUsername = view.findViewById(R.id.username_login_form);
        final EditText txtPassword = view.findViewById(R.id.password_login_form);
        final SessionManager session = new SessionManager(getActivity().getApplicationContext());



        btnToLogin.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {

                // Get username, password from EditText
                String username = txtUsername.getText().toString();
                String password = txtPassword.getText().toString();

                // Check if username, password is filled
                if(username.trim().length() > 0 && password.trim().length() > 0){
                    // For testing purpose username, password is checked with sample data
                    // username = test
                    // password = test
                    if(username.equals("test") && password.equals("test")){

                        // Creating user login session
                        // For testing i am stroing name, email as follow
                        // Use user real data
                        session.createLoginSession("Android Hive", "anroidhive@gmail.com");
                        Toast.makeText(getActivity().getApplicationContext(),
                                "User Login Status: " + txtUsername.getText().toString(),
                                Toast.LENGTH_LONG).show();
                        // Starting MainActivity
                        Intent i = new Intent(getActivity().getApplicationContext(), MainActivity.class);
                        startActivity(i);

                    }else{
                        // username / password doesn't match

                    }
                }else{
                    // user didn't entered username or password
                    // Show alert asking him to enter the details

                }


            }
        });

        btnToRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RegisterFragment registerFragment = new RegisterFragment();
                FragmentTransaction transaction = getFragmentManager().beginTransaction();

                transaction.replace(R.id.fragment_container, registerFragment);
                transaction.addToBackStack(null);

                transaction.commit();
            }
        });

        // facebook login button callback
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // if succeed, loginResult param has the new AccessToken
//                callbackManager.onActivityResult();

                // di dalam parameter loginResult akan mengandung AccessToken,
                if (AccessToken.getCurrentAccessToken() != null && !AccessToken.getCurrentAccessToken().isExpired())
                {
                    Intent i = new Intent(getContext(), MainActivity.class);
                    startActivity(i);
                }
            }

            @Override
            public void onCancel() {
                // code
            }

            @Override
            public void onError(FacebookException error) {
                Log.d("Debug", error.toString());
            }

        });

        return view;
    }

    public void addButtonListener(){

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }
}
